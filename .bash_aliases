# aliases
alias nano='nano -c'
alias cp="cp -i"
alias rm="rm -i"
alias mv="mv -i"
alias z="suspend"
alias ls='ls --color=auto'
alias la="ls -hlaF"
alias lf="ls -F"
alias lfa="ls -AF"
alias ll="ls -glh"
alias lla="ls -agl"
alias lsa='ls -1AFGh --color'    



# ssh
alias os="ssh parkj6@os-class.engr.oregonstate.edu"
alias os2="ssh parkj6@os2.engr.oregonstate.edu"
alias school='ssh parkj6@access.engr.oregonstate.edu'
alias flip1="ssh parkj6@flip1.engr.oregonstate.edu"
alias flip2="ssh parkj6@flip2.engr.oregonstate.edu"
alias flip3="ssh parkj6@flip3.engr.oregonstate.edu"


# cd
alias up="cd .."
alias cd..="cd .."
#alias home="cd /nfs/stak/students/p/parkj6/Dropbox/"
alias class="cd ~/Dropbox/_class"
alias 1="cd assignment1" || "cd HW1"
alias 2="cd assignment2" || "cd HW2"
alias 3="cd assignment3" || "cd HW3"
alias 4="cd assignment4" || "cd HW4"
alias 5="cd assignment5" || "cd HW5"
alias 6="cd assignment6" || "cd HW6"
alias 7="cd assignment7" || "cd HW7"
alias 8="cd assignment8" || "cd HW8"
alias 9="cd assignment9" || "cd HW9"
alias 10="cd assignment10" || "cd HW10"

# etc
alias me='ps -u $USER'
alias dropw="ps -u parkj6 | grep dropbox"		#where is dropbox
alias dropd="nohup ~/.dropbox-dist/dropboxd &"		#dropbox daemon

# apt
alias new="sudo apt update && sudo apt upgrade -y"


# terminal
alias title="echo -e '\033]2;' $t '\007'"
t="Default Terminal" # Default terminal title
#alias make='make -B'
