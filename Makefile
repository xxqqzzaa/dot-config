#!/bin/bash

default: copy

# copy the dot config files to home directory.
copy: bash git vim ssh

bash:
	ln -s $(pwd)/bash/.bash_aliases ~/.bash_aliases
	ln -s $(pwd)/bash/.bashrc ~/.bashrc
	ln -s $(pwd)/bash/.inputrc
	#ln -s .bash_profile ~/.bash_profile #.bashrc encouraged
	echo -e "bashrc, alias, inputrc copied over"

git:
	ln -s $(pwd)/git/.git-completion.bash ~/.git-completion.bash
	ln -s $(pwd)/git/.git-prompt.sh ~/.git-prompt.sh
	ln -s $(pwd)/git/.git-credentials ~/.git-credentials
	echo -e "git completion, prompt, credentials copied over"

vim:
#	echo "I don't have a vimrc yet."
#	ln -s .vimrc ~/.vimrc
	
ssh:
#	echo -e "It's not ready"
	
clean:
	rm ~/.bashrc ~/.bash_aliases ~/.git-completion ~/.git-prompt ~/.inputrc

