filetype plugin indent on
colo desert
syntax on
set title

set tabstop=4
set softtabstop=4
set shiftwidth=8
set noexpandtab
